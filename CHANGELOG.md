# Changelog

All notable changes to this project will be documented in this file. See
[standard-version](https://github.com/conventional-changelog/standard-version)
for commit guidelines.

## 0.0.0 (2022-01-21)

### Chore

- initial commit
  ([ef3db88](https://gitlab.com/andrea_me/pipesapp/-/commit/ef3db889451d545980fdba07408410098c86f216))
