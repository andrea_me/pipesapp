# PipesApp

> This project contains a hook that validate `lint` project and `prettier` rules
> every run the command `git commit -m '...'`

## Prerequisites

1. Node > 16.13.2
2. npm > 8.1.2
3. Install Angular CLI > 13.1.4

## Installation

1. Install dependencies

```shell
  npm ci
```

2. Start a dev server and opens in a browser

```shell
  ng serve -o
```

## Author

👤 **Andrea Morales**
